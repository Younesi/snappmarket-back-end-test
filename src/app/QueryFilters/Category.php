<?php

namespace App\QueryFilters;

class Category extends Filter
{
    protected function applyFilters($builder)
    {
        $search = request($this->filterName());
        return $builder->where('category', 'like', '%' . $search . '%');
    }
}
