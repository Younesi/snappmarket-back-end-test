<?php

namespace App\QueryFilters;

class Active extends Filter
{
    protected function applyFilters($builder)
    {
        return $builder->where('is_active', request($this->filterName()));
    }
}
