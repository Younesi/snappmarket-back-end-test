<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['name', 'product', 'price', 'desc', 'count', 'user_id', 'active'];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'active',
    ];
}
