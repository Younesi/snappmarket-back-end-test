<?php

namespace App\Http\Controllers\Admin;

use Validator;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Services\ProductService;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ProductStoreRequest;

class ProductController extends Controller
{
    protected $productService;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function store(ProductStoreRequest $request)
    {
        $result = $this->productService->storeProduct($request);
        return response()->json(['success' => $result]);
    }
}
