<?php

namespace App\Utility;

use Illuminate\Support\Facades\Storage;

class CSVHandler
{
    public function getCsvData($request)
    {
        $path = FileUploader::upload($request);

        $file = Storage::get($path);
        $csvLines = explode(PHP_EOL, $file);

        // unset headers
        $header = explode(',', $csvLines[0]);
        $header = array_map('trim', $header);
        unset($csvLines[0]);

        $products = [];
        foreach ($csvLines as $csvLine) {
            if (!empty($csvLine)) {
                $product = explode(',', $csvLine);
                if (!empty($product)) {
                    $products[] = array_combine($header, $product);
                }
            }
        }
        return array_filter($products);
    }
}
