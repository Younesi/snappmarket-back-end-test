<?php

namespace App\Utility;

class FileUploader
{
    public static function upload($request)
    {
        // Get filename with the extension
        $filenameWithExt = $request->file('csv_import')->getClientOriginalName();
        //Get just filename
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        // Get just ext
        $extension = $request->file('csv_import')->getClientOriginalExtension();
        // Filename to store
        $fileNameToStore = $filename.'_'.time().'.'.$extension;
        
        $path = $request->file('csv_import')->storeAs('cvfiles', $fileNameToStore);
        return $path;
    }
}
