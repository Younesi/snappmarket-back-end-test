<?php


namespace App\Services;

use App\Models\Product;
use App\Utility\CSVHandler;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\Storage;

class ProductService
{
    protected $model;
    protected $csvHandler;

    public function __construct(
        Product $product,
        CSVHandler $csvHandler
    ) {
        $this->model = $product;
        $this->csvHandler = $csvHandler;
    }

    public function getProducts()
    {
        $products = app(Pipeline::class)
            ->send($this->model->query())
            ->through([
                \App\QueryFilters\Category::class,
                \App\QueryFilters\Sort::class,
            ])
            ->thenReturn()
            ->paginate(5);

        $products->appends(request()->input());

        return $products;
    }

    public function storeProduct($request)
    {
        $products = $this->csvHandler->getCsvData($request);

        $result = Product::insert($products);

        return $result;
    }
}
