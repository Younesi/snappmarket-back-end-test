<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'category' => $this->faker->name(),
            'is_active' => random_int(0, 1),
            'price' => $this->faker->numberBetween(10000, 1000000),
            'count' => $this->faker->numberBetween(0, 999),
            'description' => $this->faker->sentence(),
            'user_id' => function () {
                return User::factory()->create()->id;
            },
        ];
    }
}
