<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Product;
use Illuminate\Http\Response;
use App\Constants\RbacConstant;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function setUp(): void
    {
        parent::setUp();
        $user = User::factory()->create();
        $this->artisan('db:seed');
        $this->signIn($user);
    }

    /**
     * @return void
     */
    public function test_admin_can_upload_csv_files()
    {
        $this->assertTrue(true);
    }

    /**
     * @return void
     */
    public function test_non_authenticated_users_can_see_list_of_products()
    {
        $this->post('api/auth/logout');
        $response = $this->get('api/products');
        
        $product = Product::first();
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonFragment(['name' => $product->name]);
    }
}
