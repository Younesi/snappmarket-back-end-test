<?php

namespace Tests;

use App\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /** @var  User */
    protected $loggedInUser;

    protected function signIn($user = null)
    {
        $this->loggedInUser = $user ?: create(User::class);

        $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.JWTAuth::fromUser($this->loggedInUser)
        ]);

        return $this;
    }
}
