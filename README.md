Install project:

# Run services:
CURRENT_UID=$(id -u):$(id -g) docker-compose up --detach --build

# Fix permission services:
sudo chown -R $(id -u):$(id -g) {./php/,./nginx/,./mysql/,./src/}

## Git:
docker run --rm --interactive --tty --mount="type=bind,source=$(pwd),target=/project" --user $(id -u):$(id -g) --workdir=/project alpine/git:v2.24.1

## Composer:
docker-compose exec --user $(id -u):$(id -g) app composer


## Logs:
PHP Log:
docker-compose logs --tail 100 --follow app

Nginx Log:
docker-compose logs --tail 100 --follow webserver

MySQL Log:
docker-compose logs --tail 100 --follow db



## Fix Laravel log permission:
docker-compose exec --user root app chown -R www-data:www-data /var/www/storage

## Laravel artisan:
docker-compose exec --user $(id -u):$(id -g) app php artisan

## Project URL is:
http://127.0.1.1:8080

## APi Docs:
http://127.0.1.1:8080/docs
